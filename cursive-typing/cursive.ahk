#SingleInstance force

Menu, Tray, Icon, cursive-a-yellow.png

mode := "ALPHABET"
cursive_capital := []
cursive_small := []
bold_cursive_capital := []
bold_cursive_small := []

SetCursiveCharacters()

; F2::Reload

F5:: SwitchMode("ALPHABET")
F6:: SwitchMode("BOLD_CURSIVE")
F7:: SwitchMode("LIGHT_CURSIVE")

SwitchMode(new_mode)
{
    global mode

    mode := new_mode

    if (mode = "ALPHABET") 
    {
        Menu, Tray, Icon, cursive-a-off.png
    }
    else 
    {
        Menu, Tray, Icon, cursive-a-on.png
    }

    Tooltip, Switched to %mode%
    Sleep 1000
    Tooltip,
}

; Skip hotkeys if you are typing normal
#If mode != "ALPHABET"
    ; Lowercase characters
    a::  Small(1)
    b::  Small(2)
    c::  Small(3)
    d::  Small(4)
    e::  Small(5)
    f::  Small(6)
    g::  Small(7)
    h::  Small(8)
    i::  Small(9)
    j::  Small(10)
    k::  Small(11)
    l::  Small(12)
    m::  Small(13)
    n::  Small(14)
    o::  Small(15)
    p::  Small(16)
    q::  Small(17)
    r::  Small(18)
    s::  Small(19)
    t::  Small(20)
    u::  Small(21)
    v::  Small(22)
    w::  Small(23)
    x::  Small(24)
    y::  Small(25)
    z::  Small(26)
    ; Uppercase characters SHIFT + x
    +a:: Caps(1)
    +b:: Caps(2)
    +c:: Caps(3)
    +d:: Caps(4)
    +e:: Caps(5)
    +f:: Caps(6)
    +g:: Caps(7)
    +h:: Caps(8)
    +i:: Caps(9)
    +j:: Caps(10)
    +k:: Caps(11)
    +l:: Caps(12)
    +m:: Caps(13)
    +n:: Caps(14)
    +o:: Caps(15)
    +p:: Caps(16)
    +q:: Caps(17)
    +r:: Caps(18)
    +s:: Caps(19)
    +t:: Caps(20)
    +u:: Caps(21)
    +v:: Caps(22)
    +w:: Caps(23)
    +x:: Caps(24)
    +y:: Caps(25)
    +z:: Caps(26)
#If

Caps(index) 
{
    global mode, bold_cursive_capital, cursive_capital

    if (mode = "BOLD_CURSIVE") {
        FUNC_SEND_UNICODE(bold_cursive_capital[index])
    }
    else if (mode = "LIGHT_CURSIVE") {
        FUNC_SEND_UNICODE(cursive_capital[index])
    }
}

Small(index) 
{
    global mode, bold_cursive_small, cursive_small

    if (mode = "BOLD_CURSIVE") {
        FUNC_SEND_UNICODE(bold_cursive_small[index])
    }
    else if (mode = "LIGHT_CURSIVE") {
        FUNC_SEND_UNICODE(cursive_small[index])
    }
}


; original author: JMI Madison
; https://stackoverflow.com/questions/36563635/autohotkey-send-5-digit-hex-unicode-chars
FUNC_SEND_UNICODE(index)
{
    if (StrLen(index) < 5)
    {
        Send % Chr(index)
        Return
    }

    ;chunk of data that needs to be cut into two chunks.
    ful := index + 0

    ;Subtract 0x10000 from ful, gets number 
    ;in range(rng) 0x0 to 0xFFFFFF inclusive
    rng := ful - 0x10000
    if (rng > 0xFFFFF)
    {
        msgBox,[Step1 Resulted In Out Of Range]
    }

    ;Do shifting and masking, then check to make
    ;sure the value is in the expected range:
    big_shif := (rng >>   10)
    lit_mask := (rng & 0x3FF)
    if (big_shif > 0x3FF)
    {
        msgBox,[MATH_ERROR:big_shif]
    }
    if (lit_mask > 0x3FF)
    {
        msgBox,[MATH_ERROR:lit_mask]
    }

    big := big_shif + 0xD800
    lit := lit_mask + 0xDC00
    if (big < 0xD800 or big >= 0xDBFF){
        msgBox, [HIGH_SURROGATE_OUT_OF_BOUNDS]
    }
    if (lit < 0xDC00 or lit >= 0xDFFF){
        msgBox, [LOW_SURROGATE_OUT_OF_BOUNDS]
    }
    
    ;Convert code points to actual characters:
    u1 := Chr( big )
    u2 := Chr( lit )

    ;concatentate those two characters to
    ;create our final surrogate output:
    str := u1 . u2

    Send %str%

    ;set it equal to clipboard, and send
    ;the clipboard. This is a hack.
    ;send,%str% works fine in google chrome,
    ;but fails in notepad++ for some reason.
    ;tried appending EOF, STX, ETX control codes
    ;along with output, but to no effect.
    ; clipboard := str
    ; send, ^v

    ;Must sleep before restoring clipboard,
    ;otherwise, the clipboard will get
    ;overwritten before ctrl+v has the chance
    ;to output the character. You'll end up just
    ;pasting whatever was originally on the
    ;clipboard.
    ; sleep,50
    ; clipboard := bk

    return
}

; https://unicode-search.net/unicode-namesearch.pl?term=SCRIPT
SetCursiveCharacters() {
    global 

    cursive_small := [ 0x1D4B6
        , 0x1D4B7
        , 0x1D4B8
        , 0x1D4B9
        , 0x212F
        , 0x1D4BB
        , 0x210A
        , 0x1D4BD
        , 0x1D4BE
        , 0x1D4BF
        , 0x1D4C0
        , 0x1D4C1
        , 0x1D4C2
        , 0x1D4C3
        , 0x2134
        , 0x1D4C5
        , 0x1D4C6
        , 0x1D4C7
        , 0x1D4C8
        , 0x1D4C9
        , 0x1D4CA
        , 0x1D4CB
        , 0x1D4CC
        , 0x1D4CD
        , 0x1D4CE
        , 0x1D4CF ]

    cursive_capital := [ 0x1D49C
        , 0x212C
        , 0x1D49E
        , 0x1D49F
        , 0x2130
        , 0x2131
        , 0x1D4A2
        , 0x210B
        , 0x2110
        , 0x1D4A5
        , 0x1D4A6
        , 0x2112
        , 0x2133
        , 0x1D4A9
        , 0x1D4AA
        , 0x1D4AB
        , 0x1D4AC
        , 0x211B
        , 0x1D4AE
        , 0x1D4AF
        , 0x1D4B0
        , 0x1D4B1
        , 0x1D4B2
        , 0x1D4B3
        , 0x1D4B4
        , 0x1D4B5 ]

    bold_cursive_capital := [ 0x1D4D0
        , 0x1D4D1
        , 0x1D4D2
        , 0x1D4D3
        , 0x1D4D4
        , 0x1D4D5
        , 0x1D4D6
        , 0x1D4D7
        , 0x1D4D8
        , 0x1D4D9
        , 0x1D4DA
        , 0x1D4DB
        , 0x1D4DC
        , 0x1D4DD
        , 0x1D4DE
        , 0x1D4DF
        , 0x1D4E0
        , 0x1D4E1
        , 0x1D4E2
        , 0x1D4E3
        , 0x1D4E4
        , 0x1D4E5
        , 0x1D4E6
        , 0x1D4E7
        , 0x1D4E8
        , 0x1D4E9 ]

    bold_cursive_small := [ 0x1D4EA
        , 0x1D4EB 
        , 0x1D4EC
        , 0x1D4ED
        , 0x1D4EE
        , 0x1D4EF
        , 0x1D4F0
        , 0x1D4F1
        , 0x1D4F2
        , 0x1D4F3
        , 0x1D4F4
        , 0x1D4F5
        , 0x1D4F6
        , 0x1D4F7
        , 0x1D4F8
        , 0x1D4F9
        , 0x1D4FA
        , 0x1D4FB
        , 0x1D4FC
        , 0x1D4FD
        , 0x1D4FE
        , 0x1D4FF
        , 0x1D500
        , 0x1D501
        , 0x1D502
        , 0x1D503
        , 0x1D504
        , 0x1D505 ]
}