; GemCraft: Frostborn Wrath AHK bot
; for Walden's Automation
;
; Run after entering and exiting A1 and gemcraft is in windowed mode

Pause:: Reload
`::
    Loop {
        STARTING_GEM_GRADE := 5

        ; Coordinate of where you put amplifiers
        AMPLIFIERS := []
        AMPLIFIERS.Push([ 530, 570 ])
        AMPLIFIERS.Push([ 420, 570 ])
        AMPLIFIERS.Push([ 420, 670 ])
        AMPLIFIERS.Push([ 530, 670 ])

        ; Coordinates of where you put towers
        TOWERS := []
        TOWERS.Push([ 300, 200 ]) ; Wizard hunter destroying tower
        TOWERS.Push([ 470, 570 ])
        TOWERS.Push([ 470, 620 ])
        TOWERS.Push([ 470, 670 ])
        TOWERS.Push([ 420, 620 ])
        TOWERS.Push([ 530, 620 ])

        ; Click on A1
        Click 971, 577
        Sleep 800
        
        ; Click on start battle
        Click 1433, 909

        ; wait for game loading
        Sleep 5000

        send a
        Sleep 100

        for index, amplifier in AMPLIFIERS {
            x := amplifier[1]
            y := amplifier[2]
            Click %x%, %y%
        }

        Sleep 100

        send t
        Sleep 100
        for index, tower in TOWERS {
            x := tower[1]
            y := tower[2]
            Click %x%, %y%
        }
        Sleep 100

        ; Set gem level
        Loop, % (STARTING_GEM_GRADE - 1) {
            Click 1838, 426
            Sleep 100
        }

        ; Create gems
        Loop, 9 {
            Send {Numpad4}
        }

        ; Put gems in amplifiers
        Sleep 100
        for index, amplifier in AMPLIFIERS {
            x := amplifier[1]
            y := amplifier[2]
            Click %x%, %y%
        }

        ; we are placing 10 gems, so create another one
        Send {Numpad4}

        ; Put gems in towers
        Sleep 100
        for index, tower in TOWERS {
            x := tower[1]
            y := tower[2]
            Click %x%, %y%
        }

        ; Retarget towers to attack special
        Sleep 100
        for index, tower in TOWERS {
            x := tower[1]
            y := tower[2]
            Click %x%, %y%, Right, down
            Sleep 100
            x := x - 25
            y := y + 25
            MouseMove, %x%, %y%, 0
            Sleep 100
            Click, up, Right
            Sleep 100
        }

        ; Stop upgrading wizard hunter tower
        TOWERS.RemoveAt(1)

        ; Start the round and fast forward
        Send qq

        ; Upgrade Amplifiers
        ; for index, amplifier in AMPLIFIERS {
        ;     Sleep 4000

        ;     x := amplifier[1]
        ;     y := amplifier[2]
        ;     MouseMove %x%, %y%, 0
        ;     Sleep 25
        ;     Send u
        ; }

        ; Upgrade loop
        Loop {
            for index, tower in TOWERS {
                x := tower[1]
                y := tower[2]
                MouseMove %x%, %y%, 0
                Sleep 25
                Send u

                Sleep 2000
            }

            ; Break out of loop and click 
            if (DetectImage("back-to-the-map.png") = 1) 
            {
                Click %FoundX%, %FoundY%
                Break   
            }
        }

        Sleep 2000

        ; Not sure why it missed sometimes... extra check for reliability
        if (DetectImage("back-to-the-map.png") = 1) 
        {
            Click %FoundX%, %FoundY%
        }

        Sleep 8000
    }

    Return


; image detect function
DetectImage(image) {
    global

    ImageSearch, FoundX, FoundY, 0 , 0, 1920, 1080, %image%
    if (ErrorLevel=0)
    {
        return 1
    }
    return 0
}