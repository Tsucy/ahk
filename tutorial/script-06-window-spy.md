# Window Spy

In this video I'm going to explain the Window Spy tool to help writing AutoHotkey scripts. I hated using this at first, but I promise understanding window spy is fundamental to controlling your mouse and your program windows. That's what we're building up to.

# Opening Window Spy

Window Spy is a tool built into AutoHotkey to give you specific information about your system to use in your scripts. You may have already passed over it without noticing. When you right click on an active AutoHotkey script in your task bar you'll see a `Open Window Spy` [!Fact check] option. Note that Window Spy is "always on top" so it will be in front of your other windows while using it. Don't be afraid to minimize it if its getting in your way.

In this video we are going to cover the top two sections in depth. The next two sections have more advanced information, and the bottom three sections are useful for GUIs which I will not be covering in this series. 

Another thing before we get started, keep in mind that the information in Window Spy is only information about the current active window. 

# Window Title, Class, and Process

Looking at the Window Title, Class, and Process section we get three pieces of information. The title of the window, the ahk_class of the window, and the ahk_exe of the window. You are already probably familiar with the title of a window. The title is the text displayed at the top of every window. I tend to use this as a sanity check to make sure that the correct window is active.

The ahk_class is a identifier in capital W Windows that tells what type of window it is. When using commands that apply to a specific window, the ahk_class is needed to tell AutoHotkey what window you want to apply the command on. 

ahk_exe is the filename of the program that created the window. Since most people are launching programs from the desktop or the start menu these days, nobody knows what exact executable any given app is. This is an easy way to find this out. Along with ahk_class, you can use ahk_exe to reference a specific window. window title, ahk_class or ahk_exe can be used to refrence a window, since there may be situations where there are multiple windows that have the same of any of these values.

As an example, let's figure out the title, ahk_class, and ahk_exe of Minecraft. The title looks familiar, Minecraft 1.14. But the ahk_class is LWJGL, and the ahk_exe is javaw.exe? [act increduolous] That's not something you would have known without Window Spy. 

In a later video I'll dicuss how to use these values in commands. 

# Mouse Position

Mouse Positioning can be hard to reason about at first. The Window Spy gives three different positions for your mouse. Let's break this down. I hope you've taken some geometry. [pause]

The very tippy top of your mouse pointer 

[do a super zoom right into the mouse cursor and animate to a grid]
[i have no idea if this is possible but I REALLY WOULD LOVE THIS]

is the true position of your mouse. It's only a single pixel on the millions of pixels on your screen right now. As you move the cursor left, right, up, and down its moving in a 2d coordinate plane where x is horizontal and y is vertical. However in this coordinate plane the origin or 0x 0y is located in the top left of the screen. The Y coordinate doesn't go negative as well. It starts at 0 and increases until it reaches the bottom of your screen. 

And it gets even more complicated as you add additional monitors. The origin will shift to the top left of the left most monitor, and the x coordinate increases further along

A big take away from this explanation is that coordinates are system dependant. If you make a script that uses hard coded values for your mouse position, even changing your monitor the next day could cause that script to break. This is why AutoHotkey uses the concept of relative positioning by default. 

What I just described to you is Screen coordinate mode. This will not change no matter what window you have active. In relative positioning however, the origin point becomes the top left corner of the active Window. This way no matter where the window is on your screen AutoHotkey can always place the mouse on the correct spot on the window. For example, the refresh button in chrome will always be in the same spot, so we can write down its relative coordinates to reference them in our scripts. Keep in mind you may have negative coordinates when using relative positioning, since it is allowed to click outside of the window. 

Even though the client coordinate mode says "recommended" next to it, I personally never use it. The idea of this mode is to focus entirely on the content, so it puts the origin at the top left of the main content, excluding the Windows title bar. In practice I find that this get's confusing as to exactly what is the title bar or not because modern programs will often heavily customize the title bar. To be clear, I recommend sticking with Relative mode in almost all situations.

You can change your coordinate mode in your script with the `CoordMode` command. `CoordMode, Mouse, Screen` sets it to the screen mode, and `CoordMode, Mouse, Relative` sets it to relative. Sometimes I even switch between different modes in the middle of my script!

That was Window Spy in AutoHotkey. Glad you made it. In the next video we're going to learn how to control the mouse using the knowledge about how to use window spy and the various coordinate modes. See ya.
